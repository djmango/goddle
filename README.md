# goddle

Go implementation of Puddle

## Getting started

This project requires Go to be installed. On Ubuntu you can just run `apt install go`.

Running it then should be as simple as:

```console
$ make
$ ./bin/goddle
```

### Testing

``make test``